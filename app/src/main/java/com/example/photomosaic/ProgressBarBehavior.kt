package com.example.photomosaic

interface ProgressBarBehavior {

    fun showProgressBar()
    fun hideProgressBar()

}
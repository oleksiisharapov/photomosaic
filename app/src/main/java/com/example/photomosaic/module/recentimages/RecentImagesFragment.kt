package com.example.photomosaic.module.recentimages

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.photomosaic.R
import kotlinx.android.synthetic.main.recent_images_layout.*

class RecentImagesFragment : Fragment(), RecentImagesContract.View, RecentImagesAdapter.RecyclerViewItemClickListener {
    lateinit var presenter: RecentImagesContract.Presenter


    private val itemList = listOf(
        "Str",
        "I woke up early",
        "wefq3fw34",
        "dsrfg545444",
        "Hee453rw",
        "wfewfwe",
        "java.class.exception"
    )

    private val adapter: RecentImagesAdapter by lazy { RecentImagesAdapter(this, null, itemList) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //presenter.attach(this)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.recent_images_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recentImagesRecyclerView.layoutManager = GridLayoutManager(context, 2)
        recentImagesRecyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.apply {
                    left = 20
                    right = 20
                    top = 20
                    bottom = 20
                }
            }
        })
        recentImagesRecyclerView.adapter = adapter
    }

    override fun recyclerViewItemClicked(view: View?, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}

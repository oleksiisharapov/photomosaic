package com.example.photomosaic.module.recentimages

import com.example.photomosaic.BaseContract

interface RecentImagesContract {

    interface Presenter :
        BaseContract.Presenter<View> {

        fun getItemCount(): Int

    }

    interface View : BaseContract.View {
    }

    interface ImageItem {
    }


}
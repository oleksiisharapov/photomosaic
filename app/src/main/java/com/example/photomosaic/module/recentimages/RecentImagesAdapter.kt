package com.example.photomosaic.module.recentimages

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.photomosaic.R

class RecentImagesAdapter(
    private var itemClickListener: RecyclerViewItemClickListener,
    private val presenter: RecentImagesContract.Presenter?,
    private val itemList: List<String>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface RecyclerViewItemClickListener {

        fun recyclerViewItemClicked(view: View?, position: Int)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       return RecentImagesVH(LayoutInflater.from(parent.context).inflate(R.layout.recent_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RecentImagesVH).bindItem(itemList[position])
    }

    inner class RecentImagesVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val textView: TextView = itemView.findViewById(R.id.recent_item_textview)
        private val imageView: ImageView = itemView.findViewById(R.id.recent_item_imageview)

        fun bindItem(str: String, bitmap: Bitmap? = null){
            textView.text = str
            imageView.setImageBitmap(bitmap)
        }
    }

}

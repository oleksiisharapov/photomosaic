package com.example.photomosaic.module.palette

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.photomosaic.BaseActivity
import com.example.photomosaic.R
import com.example.photomosaic.imageutils.ImageUtil
import kotlinx.android.synthetic.main.palette_layout.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class PaletteFragment: Fragment(), PaletteContract.View{


    lateinit var presenter: PaletteContract.Presenter

    val PERMISSION_RQUEST_CODE = 3001

    val palette  = mutableMapOf<Int, Bitmap>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.palette_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        paletteButton.setOnClickListener {
            initPalette()
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == PERMISSION_RQUEST_CODE){
            Log.d("Permissions:", permissions.asList().toString())
            Log.d("Permissions", grantResults.toString())
        }
    }

    private fun initPalette(){
        requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_RQUEST_CODE)
            Log.d("Permissions:", "initPalette")
        val telegramFolder = File(Environment.getExternalStorageDirectory().toString() + "/Telegram/Telegram Images")
        if(telegramFolder.exists()){
            try{
                val fileList = telegramFolder.list()?.filter{it != ".nomedia"}
                (activity as BaseActivity).showProgressBar()
                GlobalScope.launch {
                    withContext(Dispatchers.IO){
                        for(i in 0 until fileList!!.size step 30){
                            var bitmap = BitmapFactory.decodeFile("$telegramFolder/${fileList[i]}")
                            bitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, false)
                            palette[ImageUtil.aveRGB(0, 0, 120, 120, bitmap)] = bitmap
                        }
                    }
                    withContext(Dispatchers.Main){
                        (activity as BaseActivity).hideProgressBar()
                    }
                }
            } catch(e: Exception){
                Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
            }
        }
    }

}
package com.example.photomosaic.module.addnewimage

import android.graphics.Bitmap
import com.example.photomosaic.BaseContract

interface AddNewImageContract {

    interface Presenter :
        BaseContract.Presenter<View> {

        fun handleImageViewClick(bmp: Bitmap)
        fun handleProcessButtonClick()
    }


    interface View : BaseContract.View {

        fun drawImage(bitmap: Bitmap)

        fun getImageBitmap()


    }


}
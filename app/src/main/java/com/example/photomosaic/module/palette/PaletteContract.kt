package com.example.photomosaic.module.palette

import com.example.photomosaic.BaseContract

interface PaletteContract {

    interface Presenter : BaseContract.Presenter<View> {

    }

    interface View : BaseContract.View {

    }


}
package com.example.photomosaic.module.addnewimage

import android.app.Activity
import android.app.DownloadManager
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.example.photomosaic.BaseActivity
import com.example.photomosaic.ProgressBarBehavior
import com.example.photomosaic.R
import com.example.photomosaic.imageutils.ImageUtil
import kotlinx.android.synthetic.main.addnewimage_layout.*
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.security.Permission


class AddNewImageFragment : Fragment(), AddNewImageContract.View {

    lateinit var presenter: AddNewImageContract.Presenter

    protected val palette = mutableMapOf<Int, Bitmap>()

    private val PERMISSION_RQUEST_CODE = 3001

    private var isLoading = false

    private var bmp: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.addnewimage_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = AddNewImagePresenterImpl()
        presenter.attach(this)

        initPalette()

        addNewImageButton.setOnClickListener {
            presenter.handleProcessButtonClick()
        }
        addNewImageImageView.setOnClickListener {
            getImageBitmap()
            (activity as ProgressBarBehavior).showProgressBar()
        }
    }

    override fun drawImage(bitmap: Bitmap) {
        addNewImageImageView.setImageBitmap(bitmap)
    }

    override fun getImageBitmap() {
        val getPictureIntent =
            Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        getPictureIntent.type = "image/*"
        startActivityForResult(Intent.createChooser(getPictureIntent, "Select image:"), 4001)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 4001 && resultCode == Activity.RESULT_OK) {

            val uri = data?.data
            val fileDescriptor = context?.contentResolver?.openFileDescriptor(uri!!, "r")?.fileDescriptor
            bmp = BitmapFactory.decodeFileDescriptor(fileDescriptor)


            val bmpOptions = BitmapFactory.Options()
            bmpOptions.inJustDecodeBounds = true
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, bmpOptions)
            bmpOptions.outHeight = addNewImageImageView.height
            bmpOptions.outWidth = addNewImageImageView.width
            bmpOptions.inJustDecodeBounds = false
            bmp = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, bmpOptions)
            addNewImageImageView.setImageBitmap(bmp)
            bmp?.let {
                presenter.handleImageViewClick(it)
            }
        }
        if(requestCode == 5005 && resultCode == Activity.RESULT_OK){
            val dataExtra = data?.extras
            val dataString = data?.dataString

            Toast.makeText(context, "Datastring = $dataString", Toast.LENGTH_LONG).show()

        }
    }

    override fun onPause() {
        super.onPause()
        isLoading = false
        (activity as ProgressBarBehavior).hideProgressBar()
    }


    private fun initPalette() {
        if ((ContextCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_RQUEST_CODE)
        } else {
            Log.d("Permissions:", "initPalette")
            startActivityForResult(
                Intent(Intent.ACTION_GET_CONTENT).apply {
                    setDataAndType(
                        Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()),
                        "image/*"
                    )
                },
                5005
            )

        }
    }
    /*
            if (telegramFolder.exists()) {
                try {
                    val fileList = telegramFolder.list()?.filter { it != ".nomedia" }
                    (activity as BaseActivity).showProgressBar()
                    GlobalScope.launch {
                        withContext(Dispatchers.IO) {
                            for (i in 0 until fileList!!.size step 20) {
                                var bitmap = BitmapFactory.decodeFile("$telegramFolder/${fileList[i]}")
                                bitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, false)
                                palette[ImageUtil.aveRGB(0, 0, 120, 120, bitmap)] = bitmap
                            }
                        }
                        withContext(Dispatchers.Main) {
                            (activity as BaseActivity).hideProgressBar()
                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

*/





    inner class AddNewImagePresenterImpl : AddNewImageContract.Presenter {

        var job: Job? = null

        var resBmp: Bitmap? = null

        val coroutineContext = Dispatchers.Default

        private var view: AddNewImageContract.View? = null

        override fun handleImageViewClick(bmp: Bitmap) {

            GlobalScope.launch(coroutineContext) {
                val scaleFactor = bmp.height / bmp.width.toFloat()
                val copyBmp = bmp.copy(Bitmap.Config.ARGB_8888, true)
                val canvas = Canvas(copyBmp!!)
                val p = Paint()
                p.strokeWidth = 1f
                p.color = Color.RED

                val stepX: Int = 20
                val stepY = (20 * scaleFactor).toInt()

                for (x in 0 until copyBmp.width step stepX) {
                    canvas.drawLine(x.toFloat(), 0f, x.toFloat(), copyBmp.height.toFloat(), p)
                }

                for (y in 0 until copyBmp.height step stepY) {
                    canvas.drawLine(0f, y.toFloat(), copyBmp.width.toFloat(), y.toFloat(), p)
                }

                resBmp = copyBmp

                withContext(Dispatchers.Main) {
                    view?.drawImage(copyBmp)
                }
            }
        }

        override fun handleProcessButtonClick() {

            var btmp: Bitmap? = null

            val galleryIntent = Intent(Intent.ACTION_VIEW)
            val newFile =
                File(Environment.getExternalStorageDirectory().toString() + "/Telegram/Telegram Images/newFile.jpg")
            if(newFile.exists()){
                newFile.delete()
            }
            val outputStream = FileOutputStream(newFile)

            GlobalScope.launch {
                withContext(Dispatchers.Default) {
                    btmp = ImageUtil.createResultBitmap(ImageUtil.resolveColors(resBmp!!), palette)
                }
                withContext(Dispatchers.Main) {
                    view?.drawImage(btmp!!)
                    btmp!!.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
                    outputStream.flush()
                    outputStream.close()

                    galleryIntent.setDataAndType(
                        FileProvider.getUriForFile(
                            context!!,
                            context!!.packageName + ".provider",
                            newFile
                        ), "image/*")
                    galleryIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(galleryIntent)

                }
            }

        }

        override fun attach(view: AddNewImageContract.View) {
            this.view = view
        }


    }
}

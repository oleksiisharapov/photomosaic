package com.example.photomosaic

interface BaseContract {

    interface Presenter<in T : View> {
        fun attach(view: T)
    }

    interface View {

    }

}

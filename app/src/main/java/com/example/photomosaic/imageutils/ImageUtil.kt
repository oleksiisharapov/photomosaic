package com.example.photomosaic.imageutils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import kotlin.math.sqrt

class ImageUtil {

    companion object {

        fun aveRGB(startX: Int, startY: Int, width: Int, height: Int, imgSource: Bitmap): Int {
            var redBucket = 0
            var greenBucket = 0
            var blueBucket = 0
            var pixelCount = 0
            for (x in 0 until width) {
                for (y in 0 until height) {
                    val color = imgSource.getPixel(startX + x, startY + y)
                    pixelCount++
                    redBucket += color.red
                    greenBucket += color.green
                    blueBucket += color.blue

                }
            }
            return Color.rgb(redBucket / pixelCount, greenBucket / pixelCount, blueBucket / pixelCount)
        }

        fun resolveColors(imgSource: Bitmap): Array<IntArray> {

            var colorSet = Array(imgSource.width / 20) { IntArray(imgSource.height / 20) }
            for (i in 0 until imgSource.width / 20) {
                for (j in 0 until imgSource.height / 20) {
                    colorSet[i][j] = aveRGB(i * 20, j * 20, 20, 20, imgSource)
                }
            }
            return colorSet
        }

        fun resultBmp(colorSet: Array<IntArray>): Bitmap {
            val bitmap = Bitmap.createBitmap(colorSet.size * 20, colorSet[0].size * 20, Bitmap.Config.ARGB_8888)
            val p = Paint()
            val canvas = Canvas(bitmap)
            for (i in 0 until colorSet.size) {
                for (j in 0 until colorSet[i].size) {
                    p.color = colorSet[i][j]
                    canvas.drawRect(
                        i * 20f,
                        j * 20f,
                        (i + 1) * 20f,
                        (j + 1) * 20f,
                        p
                    )
                }
            }
            return bitmap
        }

        fun rgbDelta(color1: Int, color2: Int): Float {
            return sqrt(
                (color1.red - color2.red) * (color1.red - color2.red).toFloat() +
                        (color1.blue - color2.blue) * (color1.blue - color2.blue).toFloat() +
                        (color1.green - color2.green) * (color1.green - color2.green).toFloat()
            )
        }

        fun closestBitmap(color: Int, palette: Map<Int, Bitmap>): Bitmap {
            var distance = 200f
            var closestBmp = palette.values.first()
            palette.keys.forEach {
                val temp = rgbDelta(it, color)
                if (temp < distance) {
                    distance = temp
                    closestBmp = palette[it] ?: closestBmp
                }
            }
            return closestBmp
        }


        fun createResultBitmap(colorSet: Array<IntArray>, palette: Map<Int, Bitmap>): Bitmap {
            var resultBitmap = Bitmap.createBitmap(colorSet.size * 100, colorSet[0].size * 100, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(resultBitmap)
            val p = Paint()
            for (i in 0 until colorSet.size) {
                for (j in 0 until colorSet[0].size) {
                    val targetColor = colorSet[i][j]
                    canvas.drawBitmap(
                        closestBitmap(targetColor, palette),
                        i * 100f,
                        j * 100f,
                        p
                    )
                }
            }
            return resultBitmap
        }
    }

}

package com.example.photomosaic

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import com.example.photomosaic.module.addnewimage.AddNewImageFragment
import com.example.photomosaic.module.palette.PaletteFragment
import com.example.photomosaic.module.recentimages.RecentImagesFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.main_activity.*

class BaseActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener, ProgressBarBehavior {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentHolder, RecentImagesFragment())
            .commit()
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        bottomNavigationView.getOrCreateBadge(R.id.recent)
        val badg = bottomNavigationView.getBadge(R.id.recent)?.also {
            it.backgroundColor = ResourcesCompat.getColor(resources, R.color.colorAccent, null)
        }
        badg?.let {
            it.number = it.number + 1
        }

        supportFragmentManager.beginTransaction()
            //.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right)
            .replace(
                R.id.fragmentHolder,
                when (item.itemId) {
                    R.id.recent -> RecentImagesFragment()
                    R.id.create_new -> AddNewImageFragment()
                    R.id.palette -> PaletteFragment()
                    else -> return false
                }
            ).commit()
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }
}